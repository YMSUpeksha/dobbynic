# DobByNIC

This project extracts data from the NIC number. Only relevant to Sri lankan National Identity Cards

## Installation

Use the following command to clone test project to a local machine.

```bash
git clone https://gitlab.com/YMSUpeksha/DobByNIC.git
```

###Prerequisites


IDE - preferably IntelliJ IDEA, Eclipse | netbeans\
[JRE - ](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) Java runtime environment according to the operating system.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
TDD recommended. 

